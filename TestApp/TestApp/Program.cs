﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 1, 2, 3 };

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine(a[i]);
            }

            int[] x = a;
            x[0] = 9;

            for (int i = 0; i < x.Length; i++)
            {
                Console.WriteLine(x[i] + "x");
            }

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine(a[i] + "a");
            }

            Console.ReadKey();
        }
    }
}
